# anagram-svc#

## Building the service (from the project root dir)##
`mvn clean install`

## Running the service (from the project root dir)##
`java -jar target/anagram-svc.jar`

## Swagger ##
To access the Swagger UI interface hit the following URL: [http://localhost:3000/swagger-ui.html](http://localhost:3000/swagger-ui.html)

Notes:  This is just the default generated Swagger spec from SpringFox.  Given more time I would add better descriptions and details using Swagger annotations: [https://github.com/swagger-api/swagger-core/wiki/Annotations-1.5.X](https://github.com/swagger-api/swagger-core/wiki/Annotations-1.5.X)

## Notes##
- I am using a MultiMap for the data store for this project.  For a true production service I would probably use a Couchbase cluster for a distributed data store which would allow the service to scale out horizontally.
- When researching approaches for anagram storage I saw two hashing approaches: using products of primes and sorting the letters of the word.  
I implemented both hashing algorithms as the PrimesHashAlgorithm and the SortedLettersHashAlgorithm.  I wrote an integration test (HashAlgorithmIT) to compare the performance of the two algorithms.  The test measured the time to generate 1 million hash codes both ways and repeated the process 100 times.  Running the test multiple times showed results like the following:
`PrimesHashAlgorithm: BatchCount 100 : BatchSize 1000000 : Avg 21.21ms : Min 18.0ms : Max 39.0ms`
`SortedLettersHashAlgorithm: BatchCount 100 : BatchSize 1000000 : Avg 54.48ms : Min 47.0ms : Max 94.0ms`
PrimesHashAlgorithm was significantly faster so my service is using that algorithm by default.
To use the other algorithm you can use the hash.algorithm.name property on the java cmd line: Ex. --hash.algorithm.name=sorted