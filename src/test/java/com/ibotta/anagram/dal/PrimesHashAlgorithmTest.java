package com.ibotta.anagram.dal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class PrimesHashAlgorithmTest {
  
  PrimesHashAlgorithm ha;
  
  @Before
  public void setup() {
    ha = new PrimesHashAlgorithm();
  }
  
  @Test 
  public void testSetup() {
    assertEquals(26, ha.primes.length);
  }
  
  @Test
  public void testGeneratePrimes() {
    Integer[] primes = ha.generatePrimes(10);
    assertEquals(2, primes[0].intValue());
    assertEquals(7, primes[3].intValue());
    assertEquals(10, primes.length);
  }
  
  @Test
  public void testHashCode_Equals() {
    long hash1 = ha.getHash("dog");
    long hash2 = ha.getHash("god");
    assertEquals(hash1, hash2);
  }
  
  @Test
  public void testHashCode_Equals_MixedCase() {
    long hash1 = ha.getHash("dog");
    long hash2 = ha.getHash("Dog");
    assertEquals(hash1, hash2);
  }
  
  @Test
  public void testHashCode_NotEquals() {
    long hash1 = ha.getHash("dog");
    long hash2 = ha.getHash("good");
    assertNotEquals(hash1, hash2);
  }
  
  @Test
  public void testHashCode_Hyphen() {
    long hash1 = ha.getHash("set-up");
    long hash2 = ha.getHash("setup");
    long hash3 = ha.getHash("upset");
    assertEquals(hash1, hash2);
    assertEquals(hash2, hash3);
  }
 
}
