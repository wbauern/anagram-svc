package com.ibotta.anagram.dal;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimaps;
import com.ibotta.anagram.domain.Metrics;

public class AnagramMapDalTest {
  
  AnagramMapDal dal;
  
  @Before
  public void setup() {
    dal = new AnagramMapDal(Multimaps.synchronizedSetMultimap(HashMultimap.create()), 
        new PrimesHashAlgorithm());
  }
  
  @Test
  public void testMetrics() {
    dal.addWord("dog");
    dal.addWord("god");
    dal.addWord("dogma");
    dal.addWord("before");

    Metrics metrics = dal.getMetrics();
    assertEquals(6, metrics.getMax());
    assertEquals(3, metrics.getMin());
    assertEquals(4, metrics.getCount());
    assertEquals(17d/4d, metrics.getAvg(), .0001);
    assertEquals(4, metrics.getMedian(), .0001);
  }
  
  @Test
  public void testGetAnagrams() {
    dal.addWord("dog");
    dal.addWord("god");
    dal.addWord("God");
    dal.addWord("dogma");
    
    Set<String> anagrams = dal.getAnagrams("dog", null);
    assertEquals(2, anagrams.size());
    assertTrue(anagrams.contains("god"));
    assertTrue(anagrams.contains("God"));
  }
  
  @Test
  public void testGetAnagrams_WithLimit() {
    dal.addWord("dog");
    dal.addWord("god");
    dal.addWord("God");
    dal.addWord("dogma");
    
    Set<String> anagrams = dal.getAnagrams("dog", 1);
    assertEquals(1, anagrams.size());
  }
  
  @Test
  public void testDeleteWord() {
    dal.addWord("dog");
    dal.addWord("god");
    dal.addWord("God");
    dal.addWord("dogma");
    
    assertTrue(dal.getAnagrams("dog", null).contains("God"));
    dal.deleteWord("God");
    assertFalse(dal.getAnagrams("dog", null).contains("God"));
  }
  
  @Test
  public void testDeleteAll() {
    dal.addWord("dog");
    dal.addWord("god");
    dal.addWord("God");
    dal.addWord("dogma");
    
    dal.deleteAll();
    assertEquals(0, dal.getMetrics().getCount());
  }
}
