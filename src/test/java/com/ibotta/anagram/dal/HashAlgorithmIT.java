package com.ibotta.anagram.dal;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.boot.logging.LoggingSystem;

public class HashAlgorithmIT {

  private static final Logger LOGGER = LoggerFactory.getLogger(HashAlgorithmIT.class);
  
  @BeforeClass
  public static void setErrorLogging() {
     LoggingSystem.get(ClassLoader.getSystemClassLoader()).setLogLevel(Logger.ROOT_LOGGER_NAME, LogLevel.INFO);
  }
  
  @Test 
  public void compareHashPerformance() {
    int testCount = 1000000;
    String word = "qbdcezfgc";
    
    DescriptiveStatistics primesStats = new DescriptiveStatistics();
    DescriptiveStatistics sortedLetterStats = new DescriptiveStatistics();

    for (int i=0; i<100; i++) {
      long primesDuration = generatePrimesHashAlgorithm(testCount, word);
      
      long sortedLetterDuration = generateSortedLettersAlgorithm(testCount, word);
          
      if (primesDuration < sortedLetterDuration) {
        LOGGER.debug("PrimesHashAlgorithm wins by {}ms", sortedLetterDuration-primesDuration);
      } else {
        LOGGER.debug("SortedLettersHashAlgorithm wins by {}ms", primesDuration-sortedLetterDuration);
      }
      
      primesStats.addValue(primesDuration);
      sortedLetterStats.addValue(sortedLetterDuration);
    }
    
    LOGGER.info("PrimesHashAlgorithm: BatchCount {} : BatchSize {} : Avg {}ms : Min {}ms : Max {}ms", 
        primesStats.getN(), 
        testCount,
        primesStats.getMean(),
        primesStats.getMin(),
        primesStats.getMax()
        );
    
    LOGGER.info("SortedLettersHashAlgorithm: BatchCount {} : BatchSize {} : Avg {}ms : Min {}ms : Max {}ms", 
        sortedLetterStats.getN(), 
        testCount,
        sortedLetterStats.getMean(),
        sortedLetterStats.getMin(),
        sortedLetterStats.getMax()
        );
    
  }
  
  private long generatePrimesHashAlgorithm(int testCount, String word) {
    HashAlgorithm alg = new PrimesHashAlgorithm();
    long start = System.currentTimeMillis();
    for (int i=0; i<testCount; i++) {
      long hash = alg.getHash(word);
    }
    long duration = System.currentTimeMillis() - start;
    LOGGER.debug("PrimesHashAlgorithm: {}ms", duration);
    return duration;
  }
  
  private long generateSortedLettersAlgorithm(int testCount, String word) {
    HashAlgorithm alg = new SortedLettersHashAlgorithm();
    long start = System.currentTimeMillis();
    for (int i=0; i<testCount; i++) {
      long hash = alg.getHash(word);
    }
    long duration = System.currentTimeMillis() - start;
    LOGGER.debug("SortedLettersHashAlgorithm: {}ms", duration);
    return duration;
  }
  
}
