package com.ibotta.anagram.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ibotta.anagram.service.AnagramUtils;

public class AnagramUtilsTest {
  
  @Test
  public void testSortedKey() {
    assertEquals("dgo", AnagramUtils.sortedKey("dog"));
    assertEquals("dgo", AnagramUtils.sortedKey("god"));
  }
}
