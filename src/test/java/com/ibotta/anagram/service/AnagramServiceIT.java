package com.ibotta.anagram.service;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.ibotta.anagram.domain.Anagrams;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class AnagramServiceIT {
  private static final Logger LOGGER = LoggerFactory.getLogger(AnagramServiceIT.class);
  
  @Autowired
  private AnagramService service;
  
  @Test
  public void testDictionaryLoad() throws IOException {
    service.loadDictionary();
    Anagrams anagrams = service.getAnagrams("dog", null);
    anagrams.getAnagrams().forEach(w -> LOGGER.info("anagram {}", w));
  }
}
