package com.ibotta.anagram.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ibotta.anagram.domain.Anagrams;
import com.ibotta.anagram.service.AnagramServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(AnagramController.class)
public class AnagramControllerTest {

  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private AnagramServiceImpl anagramService;
  
  @Test
  public void testGetAnagrams() throws Exception {
    Anagrams anagrams = Anagrams.builder().anagram("oof").build();
    given(anagramService.getAnagrams("foo", null)).willReturn(anagrams);
    mockMvc.perform(
      get("http://localhost:8080//anagrams/foo.json")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string("{\"anagrams\":[\"oof\"]}"));
  }
  
  @Test
  public void testDeleteAll() throws Exception {
    mockMvc.perform(
      delete("http://localhost:8080//words.json")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent());
    
    verify(anagramService).deleteAllWords();
  }

}
