package com.ibotta.anagram.domain;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WordsTest {
  private static ObjectMapper om = new ObjectMapper();
  
  @Test
  public void testRead() throws JsonParseException, JsonMappingException, IOException {
    String json = "{ \"words\": [\"read\",\"write\"] }";
    Words words = om.readValue(json, Words.class);
    assertEquals(2, words.getWords().size());
  }
  
  @Test
  public void testWrite_Empty() throws JsonParseException, JsonMappingException, IOException {
    String expectedJson = "{\"words\":[]}";
    Words words = Words.builder().clearWords().build();
    String json = om.writeValueAsString(words);
    assertEquals(expectedJson, json);
  }
}
