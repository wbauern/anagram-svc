package com.ibotta.anagram.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(builder = LoadMetrics.LoadMetricsBuilder.class)
public class LoadMetrics {
  long loadTimeMs;
  Metrics metrics;

  @JsonPOJOBuilder(withPrefix = "")
  public static final class LoadMetricsBuilder {
  }
}
