package com.ibotta.anagram.domain;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;

@Value
@Builder
@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(builder = Anagrams.AnagramsBuilder.class)
@JsonRootName(value = "anagrams")
public class Anagrams {
  
  @Singular
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private Set<String> anagrams;
	
	@JsonPOJOBuilder(withPrefix = "")
	public static final class AnagramsBuilder {
	}
}
