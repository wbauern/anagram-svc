package com.ibotta.anagram.domain;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(builder = Metrics.MetricsBuilder.class)
public class Metrics {
  private long count;
  private double avg;
  private long min;
  private long max;
  private double median;
  
  @JsonPOJOBuilder(withPrefix = "")
  public static final class MetricsBuilder {
  }
}
