package com.ibotta.anagram.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.ibotta.anagram.dal.HashAlgorithm;
import com.ibotta.anagram.dal.PrimesHashAlgorithm;
import com.ibotta.anagram.dal.SortedLettersHashAlgorithm;

@Configuration
public class AnagramConfig {
  
  @Bean
  @ConditionalOnProperty(name = "hash.algorithm.name", havingValue = "primes", matchIfMissing = true)
  public HashAlgorithm getPrimesHashAlgorithm() {
    return new PrimesHashAlgorithm();
  }
  
  @Bean
  @ConditionalOnProperty(name = "hash.algorithm.name", havingValue = "sorted")
  public HashAlgorithm getSortedLettersHashAlgorithm() {
    return new SortedLettersHashAlgorithm();
  }
  
  @Bean
  public Multimap<Integer,String> getMultiMap() {
    return Multimaps.synchronizedSetMultimap(HashMultimap.create());
  }
  
}
