package com.ibotta.anagram.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.ibotta.anagram.dal.AnagramDal;
import com.ibotta.anagram.domain.Anagrams;
import com.ibotta.anagram.domain.LoadMetrics;
import com.ibotta.anagram.domain.Metrics;

@Service
public class AnagramServiceImpl implements AnagramService {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AnagramServiceImpl.class);

  @Autowired
  AnagramDal anagramDal;
  
  @PostConstruct
  private void setup() throws IOException {
    loadDictionary();
  }
  
  @Override
  public LoadMetrics loadDictionary() throws IOException {
    deleteAllWords();
    
    LOGGER.info("Starting dictionary load...");
    long startMillis = System.currentTimeMillis();

    Resource resource = new ClassPathResource("dictionary.txt");
    InputStream dictionaryStream = resource.getInputStream();
    BufferedReader br = new BufferedReader(new InputStreamReader(dictionaryStream));
    Stream<String> stream = br.lines();

    stream.forEach(w -> {LOGGER.debug("word {}", w); anagramDal.addWord(w); });
    
    long loadTimeMs = System.currentTimeMillis()-startMillis;
    LOGGER.info("Finished dictionary load: total ms {}", loadTimeMs);
    Metrics metrics = getMetrics();
    LOGGER.info("Anagram map metrics: {}", metrics);
    
    return LoadMetrics.builder().metrics(metrics).loadTimeMs(loadTimeMs).build();
  }
  
  @Override
  public Anagrams getAnagrams(String word, Integer maxCount) {
    return Anagrams.builder().anagrams(anagramDal.getAnagrams(word, maxCount)).build();
  }

  @Override
  public void addWords(Set<String> words) {
    words.forEach(w -> anagramDal.addWord(w));
  }

  @Override
  public void deleteWord(String word) {
    anagramDal.deleteWord(word);
  }

  @Override
  public void deleteAllWords() {
    anagramDal.deleteAll();
  }
  
  @Override
  public Metrics getMetrics() {
    return anagramDal.getMetrics();
  }

  @Override
  public Metrics getHashMetrics() {
    return anagramDal.getHashMetrics();
  }

}
