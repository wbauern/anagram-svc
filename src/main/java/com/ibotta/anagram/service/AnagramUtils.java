package com.ibotta.anagram.service;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnagramUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(AnagramUtils.class);
  
  public static String sortedKey(String word) {
    char[] characters = word.toCharArray();

    Arrays.sort(characters);
    return String.valueOf(characters);
  }

}
