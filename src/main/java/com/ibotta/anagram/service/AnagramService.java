package com.ibotta.anagram.service;

import java.io.IOException;
import java.util.Set;

import com.ibotta.anagram.domain.Anagrams;
import com.ibotta.anagram.domain.LoadMetrics;
import com.ibotta.anagram.domain.Metrics;

public interface AnagramService {

  LoadMetrics loadDictionary() throws IOException;

  Anagrams getAnagrams(String word, Integer maxCount);

  void addWords(Set<String> words);

  void deleteWord(String word);

  void deleteAllWords();

  Metrics getMetrics();

  Metrics getHashMetrics();

}