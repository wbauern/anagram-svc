package com.ibotta.anagram.dal;

public interface HashAlgorithm {
  public int getHash(String word);
}
