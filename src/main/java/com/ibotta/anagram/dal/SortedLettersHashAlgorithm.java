package com.ibotta.anagram.dal;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SortedLettersHashAlgorithm implements HashAlgorithm {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(SortedLettersHashAlgorithm.class);
  
  public SortedLettersHashAlgorithm() {
  }
  
  @Override
  public int getHash(String word) {
    char[] charArray = word.toCharArray();
    Arrays.sort(charArray);
    String sortedWord = new String(charArray);
    return sortedWord.hashCode();
  }
    
}
