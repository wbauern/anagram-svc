package com.ibotta.anagram.dal;

import java.util.Set;

import com.ibotta.anagram.domain.Metrics;

public interface AnagramDal {
  public void addWord(String word);
  public Set<String> getAnagrams(String word, Integer maxCount);
  public void deleteWord(String word);
  public void deleteAll();
  
  // Optional
  public Metrics getMetrics();
  Metrics getHashMetrics();  
  
  
}
