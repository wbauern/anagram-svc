package com.ibotta.anagram.dal;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Multimap;
import com.ibotta.anagram.domain.Metrics;

@Component
public class AnagramMapDal implements AnagramDal {
  
  Multimap<Integer,String> anagramMap;
  HashAlgorithm hashAlgorithm;
  
  @Autowired
  public AnagramMapDal(Multimap<Integer,String> anagramMap, HashAlgorithm hashAlgorithm) {
    this.anagramMap = anagramMap;
    this.hashAlgorithm = hashAlgorithm;
  }
  
  @Override
  public void addWord(String word) {
    String lcw = word.toLowerCase();
    anagramMap.put(hashAlgorithm.getHash(lcw), word);
  }

  @Override
  public Set<String> getAnagrams(String word, Integer maxCount) {
    Set<String> anagrams = new HashSet<>();
    String lcw = word.toLowerCase();
    int currentCount = 0;
    for (String anagram : anagramMap.get(hashAlgorithm.getHash(lcw))) {
      if (!anagram.equals(lcw) && (maxCount == null || currentCount < maxCount)) {
        anagrams.add(anagram);
        currentCount++;
      }
    }
    return anagrams;
  }

  @Override
  public void deleteWord(String word) {
    String lcw = word.toLowerCase();
    anagramMap.remove(hashAlgorithm.getHash(lcw), word);
  }

  @Override
  public void deleteAll() {
    anagramMap.clear();
  }

  @Override
  public Metrics getMetrics() {
    DescriptiveStatistics stats = new DescriptiveStatistics();
    anagramMap.values().forEach(w -> {stats.addValue(w.length());});
    return Metrics.builder().count(stats.getN())
        .avg(stats.getMean())
        .median(stats.getPercentile(50))
        .min((long)stats.getMin())
        .max((long)stats.getMax())
        .build();
  }

  @Override
  public Metrics getHashMetrics() {
    // TODO Auto-generated method stub
    return null;
  }
}
