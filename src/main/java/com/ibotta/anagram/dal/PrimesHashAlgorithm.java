package com.ibotta.anagram.dal;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrimesHashAlgorithm implements HashAlgorithm {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(PrimesHashAlgorithm.class);

  Integer[] primes;
  
  public PrimesHashAlgorithm() {
    primes = generatePrimes(26);
  }
  
  @Override
  public int getHash(String word) {
    long product = 1;
    for (int i = 0; i < word.length(); i++) {
      int charIndex = 0;
      char c = word.charAt(i);
      if (c == '-') {
        continue;
      } else if (c >= 'a' && c <= 'z') {
        charIndex = c - 'a';
      } else if (c >= 'A' && c <= 'Z') {
        charIndex = c - 'A';
      } else {
        throw new RuntimeException("Invalid character in word: word [" + word + "] , character [" + c + "]");
      }
      product = product * primes[charIndex];
    }
    if (product > Integer.MAX_VALUE) {
      LOGGER.trace("Exceeded MAX INT: {}", product);
      LOGGER.trace("Long.hashCode: {}", Long.hashCode(product));
    }
    return Long.hashCode(product);
  }
  
  Integer[] generatePrimes(int count) {
    List<Integer> primes = new ArrayList<>();
    for (int i = 2; i <= Integer.MAX_VALUE; i++) {
      boolean isPrime = true;
      for (int j = 2; j < i; j++) {
        if (i % j == 0) {
          isPrime = false;
          break;
        }
      }

      if (isPrime) {
        LOGGER.debug("Found prime: index {} letter {} prime {}", primes.size(), (char)('a' + primes.size()), i);
        primes.add(i);
        if (primes.size() >= count) {
          break;
        }
      }
    }
    return primes.toArray(new Integer[primes.size()]);
  }
    
}
