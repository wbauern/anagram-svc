package com.ibotta.anagram.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibotta.anagram.domain.Anagrams;
import com.ibotta.anagram.domain.LoadMetrics;
import com.ibotta.anagram.domain.Metrics;
import com.ibotta.anagram.domain.Words;
import com.ibotta.anagram.service.AnagramService;

@RestController
@Validated
public class AnagramController {
  private static final Logger LOGGER = LoggerFactory.getLogger(AnagramController.class);

  @Autowired
  private AnagramService anagramService;

  @PostMapping(path = "/words.json", consumes = "application/json")
  public ResponseEntity<String> addWords(@RequestBody Words words) {
    anagramService.addWords(words.getWords());
    return new ResponseEntity<String>(HttpStatus.CREATED);
  }

  @GetMapping(path = "/anagrams/{word}.json", produces = "application/json")
  public Anagrams getAnagrams(@PathVariable("word") String word, 
      @Min(0) @RequestParam(value="limit",required=false)  Integer limit) {
    return anagramService.getAnagrams(word, limit);
  }
  
  @GetMapping(path = "/corpus-metrics.json", produces = "application/json")
  public Metrics getMetrics() {
    return anagramService.getMetrics();
  }
  
  @GetMapping(path = "/hash-metrics.json", produces = "application/json")
  public Metrics getHashMetrics() {
    return anagramService.getHashMetrics();
  }

  @DeleteMapping(path = "/words/{word}.json")
  public ResponseEntity<String> deleteWord(@PathVariable("word") String word) {
    anagramService.deleteWord(word);
    return new ResponseEntity<String>(HttpStatus.OK);
  }

  @DeleteMapping(path = "/words.json")
  public ResponseEntity<String> deleteAllWords() {
    anagramService.deleteAllWords();
    return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
  }
  
  @PostMapping(path = "/load")
  public LoadMetrics load() throws IOException {
    anagramService.deleteAllWords();
    return anagramService.loadDictionary();
  }
  
  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  public ResponseEntity<ErrorResponse> handleTimeoutException(ConstraintViolationException ex) {

    List<ErrorItem> errorItems = new ArrayList<>();   
    Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
    for (ConstraintViolation<?> violation : violations ) {
      errorItems.add(ErrorItem.builder().code("bad_request").message(violation.getPropertyPath() + " " + violation.getMessage()).build());
    }
    ErrorResponse response = ErrorResponse.builder().erroritems(errorItems).status(HttpStatus.BAD_REQUEST.value())
        .build();
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }
}
